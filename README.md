# personal logo
A logo/icon/pfp I designed for myself

![Logo](logo.png)

## Generation
Running `svg2img <formats>` will generate all the variants of the logo in the formats of your choice. The logo goes from the template to SVG to PNG to whatever format you choose. It supports any format that ImageMagick supports, as well as ASCII and Unicode.

For example, `svg2img webp heic` will generate SVG, PNG, WebP and HEIF versions of all 18 color and shape variants of the logo.

### Depends
  * Inkscape (core usage)
  * ImageMagick (convert from PNG to anything)
  * jp2a (convert from PNG to ASCII)
  * chafa (convert from PNG to Unicode)

## Design Criteria
  * Written as a simple SVG
  * Looks equally good in ASCII/Unicode art
  * Monochrome
  * No words/typefaces involved

## Color palette
| color       | hex     |
|-------------|---------|
| white       | #000000 |
| dark        | #212121 |
| color       | #00FFBF |
| transparent | n/a     |

## License
Copyright 2021 Armaan Bhojwani <me@armaanb.net>. `svg2img` is MIT Licensed, however the logo itself is [not licensed](https://choosealicense.com/no-permission/), and under exclusive copyright. See the LICENSE file for more information.
